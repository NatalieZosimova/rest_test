package helpers;

import io.restassured.response.Response;

import static org.hamcrest.Matchers.*;

public class Checker {
    private final Response response;

    public Checker(Response resp) {
        this.response = resp;
    }

    public void checkCode(int code) {
        response.then()
                .log().all()
                .assertThat()
                .statusCode(code);
    }

    public void checkTime(int ms) {
        response.then()
                .log().all()
                .assertThat()
                .time(lessThan((long) ms));
    }

    public void checkJsonBody(String key, String value) {
        response.then()
                .log().all()
                .assertThat()
                .body(key, equalTo(value));
    }



}
