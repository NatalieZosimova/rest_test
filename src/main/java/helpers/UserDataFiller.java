package helpers;

import com.github.javafaker.Faker;
import java.util.Locale;
import dto.TestUser;

public class UserDataFiller {
    public static Faker faker = new Faker(new Locale("en-US"));

    public TestUser fillData(TestUser testUser) {
        testUser.setId(faker.number().numberBetween(0, 2147483646));
        testUser.setUsername(faker.name().username());
        testUser.setFirstName(faker.name().firstName());
        testUser.setLastName(faker.name().lastName());
        testUser.setEmail(faker.internet().emailAddress());
        testUser.setPassword(faker.internet().password());
        testUser.setPhone(faker.phoneNumber().cellPhone());
        testUser.setUserStatus(faker.number().randomDigit());

        return testUser;
    }
}
