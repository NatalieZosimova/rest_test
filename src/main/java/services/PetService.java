package services;

import dto.TestUser;
import io.restassured.response.Response;

public class PetService extends APIService{

    public static Response getPetById(String petId) {
        return setup()
                .when()
                .get("pet/".concat(petId));
    }

}
