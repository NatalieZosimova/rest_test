import dto.TestUser;
import helpers.Checker;
import helpers.UserDataFiller;
import org.testng.Assert;
import org.testng.annotations.Test;
import services.PetService;
import services.UserService;



public class SwaggerTest extends BaseTest{

    @Test
    public void createAndCheckUser()  {
        // create a random-generated user object
        TestUser testUser = new UserDataFiller().fillData(new TestUser());
        // call "create"-service
        UserService.createUser(testUser);
        // try to get new user
        TestUser siteUser = UserService.getUserByName(testUser.getUsername());
        // check that new user is equal to our user object
        Assert.assertEquals(testUser, siteUser);

    }

    @Test
    public void getPet() {
        // use custom class for assertions
        Checker chkResp = new Checker(
                PetService.getPetById("18"));
        chkResp.checkCode(200);
        chkResp.checkTime(200);
        chkResp.checkJsonBody("name", "doggie");

    }

}
