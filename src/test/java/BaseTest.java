
import io.restassured.RestAssured;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeSuite;


public class BaseTest {

    private String swaggerURL;
    public String getSwaggerURL() {
        return swaggerURL;
    }


    @BeforeSuite
    public void init() {
        initConfig();
        RestAssured.baseURI = getSwaggerURL();
        RestAssured.basePath = "/v2";
    }


    private void initConfig() {
            MainConfig cfg = ConfigFactory.create(MainConfig.class);
            this.swaggerURL = cfg.swaggerURL();
    }



}

